<?php

namespace App;

use App\Exceptions\EmployeeRepositoryException;
use App\Traits\SearchableTrait;
use App\Traits\SortableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    use SearchableTrait , SortableTrait;

    protected $guarded = [
        'position',
        'id',
        'avatar',
    ];

    /**/
    protected $searchableColumns = [
        'employees.id',
        'employees.first_name',
        'employees.last_name',
        'employees.middle_name',
        'employees.hired_at',
        'employees.salary',
        'employees.position',
        'parent.last_name',
        'parent.first_name',
        'parent.middle_name',
    ];

    protected $joinColumns = [
        'parent_id' => [
            'table' => 'employees',
            'primary_key' => 'id',
            'operator' => '=',
            'alias' => 'parent',
            'select' => [
                'last_name' => 'parent_last_name',
                'first_name' => 'parent_first_name',
                'middle_name' => 'parent_middle_name',
            ],
        ],
    ];

    public function getPositions(){
        $data = config('seeding.hierarchy');
        $positions = [];
        foreach ($data as $key => $value){
            $positions[$key] = $value['position'];
        }
        return $positions;
    }

    public function parent(){
        return $this->belongsTo('App\Employee' , 'parent_id');
    }

    public function children(){
        return $this->hasMany('App\Employee' , 'parent_id');
    }

    public function getFullName(){
        $names = [
            $this->last_name,
            $this->first_name,
            $this->middle_name,
        ];
        return implode(' ' , $names);
    }

    public function changeBoss($bossId = null){
        if($this->hierarchy_level === 0){
            throw new EmployeeRepositoryException('This is real BOSS');
        }
        if ($bossId){
            try{
                $boss = $this->getModel()::findOrFail($bossId);
                if($this->hierarchy_level > $boss->hierarchy_level){
                    throw new EmployeeRepositoryException('This employee can not be your boss.');
                }
            } catch (\Exception $exception){
                throw new EmployeeRepositoryException('Boss with id: ' . $bossId . ' can not be found.');
            }
        }
        else{
            $hierarchy_level = (int)$this->hierarchy_level -1;
            while (!$bossId){
                if($hierarchy_level < 0){
                    throw new EmployeeRepositoryException('Something went wrong');
                }
                $boss = DB::table($this->getTable())->where('hierarchy_level' , $hierarchy_level)->inRandomOrder()->first();
                $bossId = $boss ? $boss->id : null;
                $hierarchy_level--;
            }
        }
        $this->parent_id = $bossId;
        $this->save();
    }

    public function isRoot(){
        return (int)$this->hierarchy_level === 0;
    }
}
