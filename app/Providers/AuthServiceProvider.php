<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Routing\Route;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
        Passport::tokensCan([
            'store-employee' => 'Add employees',
            'update-employee' => 'Update employees',
            'delete-employee' => 'Delete employees',
            'get-employee' => 'Get employee or list of employees',
            'get-user' => 'Get user or list of users',
        ]);
        $routes = app('router')->getRoutes();
        $authRoute = $routes->getByName('passport.authorizations.authorize');
        $authRoute->middleware('authorize-scopes');
    }
}
