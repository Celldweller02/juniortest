<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class EmployeeRepositoryException extends Exception
{
    public function __construct(string $message = "", int $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }


    public function report(){

    }

    public function render(){
        return response()->json(['error' => $this->getMessage()] , $this->getCode());
    }
}