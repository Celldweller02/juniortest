<?php
namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Container\Container as App;
use App\Repository\Interfaces\RepositoryInterface;

abstract class Repository implements RepositoryInterface
{
    private $app;

    protected $model;

    function __construct(App $app )
    {
        $this->app = $app;
        $this->makeModel();
    }

    abstract function model();

    protected function makeModel(){
        $model = $this->app->make($this->model());

        if(!$model instanceof Model){
            throw new \Exception();
        }

        $this->model = $model;
    }

    function all($columns = ['*'])
    {
        return $this->model->all($columns);
    }

    function paginate($quantity = 100)
    {
        return $this->model->paginate($quantity);
    }

    function store(Request $request)
    {
        return $this->model->create($request);
    }

    function update(Request $request, $id , $attribute = 'id')
    {
        return $this->model->where($attribute , $id)->update($request);
    }

    function delete($id)
    {
        return $this->model->destroy($id);
    }

    function find($id , $columns = ['*'])
    {
        return $this->model->findOrFail($id , $columns);
    }

    function findBy($field, $value , $columns = ['*'])
    {
        return $this->model->where($field , $value)->get($columns);
    }


}