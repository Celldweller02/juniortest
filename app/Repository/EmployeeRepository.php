<?php
namespace App\Repository;

use App\Employee;
use App\Exceptions\EmployeeRepositoryException;
use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\ManageEmployeeRequest;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;

class EmployeeRepository extends Repository
{
    function model()
    {
        return 'App\Employee';
    }

    function getRoots(){
        return $this->model->where('hierarchy_level' , 0)->get();
    }

    function getSubordinates($id){
        $boss = $this->find($id);
        $subordinates = $boss->children;
        return $subordinates;
    }

    function manageEmployees(ManageEmployeeRequest $request){
        $query = '';
        $searchQuery = $request->get('search');
        $sortOptions = $request->get('sort');
        if($searchQuery){
            $query = $this->model->search($searchQuery);
            $query = $this->model->prepareQuery($query , $sortOptions);
        }

        if($sortOptions){
            $query = $this->model->sortQuery($sortOptions , $query);
        }

        if($query){
           return $employees = $this->model->hydrate($query->get()->toArray())->paginate(100);
        }

        return $this->paginate(100);
    }

    function prepareEmployeeForm($request){
        $id = $request->get('id');
        $positions = array_flip($this->model->getPositions());
        $bosses = [];
        $hierarchyRequest = $request->get('hierarchy_level');
        if($hierarchyRequest || $hierarchyRequest === '0'){
            return $this->getPotentialBosses($hierarchyRequest - 1);
        }

        if($id){
            $employee = $this->find($id); //try-catch needed
            $bosses = $this->getPotentialBosses($employee->hierarchy_level - 1);
            return view('sub-views.modal-form')->with([
                'employee' => $employee,
                'positions' => $positions,
                'bosses' => $bosses,
            ]);
        }

        return view('sub-views.modal-form')->with([
            'positions' => $positions,
            'bosses' => $bosses,
        ]);
    }

    function storeEmployee(EmployeeRequest $request , Employee $emp = null){
        $employee = $emp ? $emp : new Employee();
        $positions = $employee->getPositions();
        $data = $request->all();
        if(!$data['parent_id']){
            $isCEO = (int)$employee->hierarchy_level === 0;
            $setCEO = (int)$data['hierarchy_level'] === 0;
            $hasPotentialBoss = $this->findBy('hierarchy_level' , $data['hierarchy_level'] -1)->toArray();
            if(!$isCEO || !$setCEO || $hasPotentialBoss){
                throw new EmployeeRepositoryException('Please select employees boss' , 400);
            }
        }
        else{
            try{
                $this->find($data['parent_id']);
            } catch (\Exception $e){
                throw new EmployeeRepositoryException('Boss with id: ' . $data['parent_id'] . ' cannot be found' , 400);
            }
        }
        if(!in_array($data['hierarchy_level'] , array_keys($positions))){
            throw new EmployeeRepositoryException('Wrong hierarchy level' , 400);
        }
        $position = $positions[$data['hierarchy_level']];
        $positionWasChanged = (int)$data['hierarchy_level'] !== (int)$employee->hierarchy_level;
        $employee->fill($data);
        $employee->position = $position;
        if($request->hasFile('avatar')){
            try{
                unlink($employee->avatar);
            } catch (\Exception $exception){}
            $image = Image::make($request->file('avatar'));
            $path = storage_path('images/' . str_replace([' ' , ':'] , '-' , Carbon::now()) . '-' . $request->file('avatar')->getClientOriginalName());
            $image->save($path);
            $employee->avatar = $path;
        }
        $employee->save();
        if($positionWasChanged){
            $subordinates = $employee->children;
            foreach ($subordinates as $subordinate){
                $subordinate->changeBoss();
            }
        }
        return $employee;
    }

    function deleteEmployee($employee){
        $children = $employee->children;
        $employee->delete();
        foreach ($children as $child) {
            $child->changeBoss();
        }
        return true;
    }

    function changeBoss($employeeId , $bossId){
        $employee = $this->find($employeeId);
        $boss = $this->find($bossId);
        if((int)$employee->hierarchy_level - (int)$boss->hierarchy_level === 1){
            if($employee->parent_id === $boss->id){
                return response()->json(['success' => 'Employee was assigned']);
            }
            else{
                $employee->parent_id = $boss->id;
                $employee->save();
                return response()->json(['success' => 'Employee was assigned']);
            }
        }
        return response()->json(['danger' => 'Wrong id of boss or subordinate was specified']);
    }

    private function getPotentialBosses($hierarchy_level){
        if((int)$hierarchy_level < 0){
            return [
                'N/A' => 0,
            ];
        }
        $bosses = $this->findBy('hierarchy_level' , $hierarchy_level);
        while (!$bosses->toArray()){
            $hierarchy_level--;
            if($hierarchy_level < 0){
                throw new EmployeeRepositoryException('Something went wrong' , 404);
            }
            $bosses = $this->findBy('hierarchy_level' , $hierarchy_level);
        }
        foreach ($bosses as $key =>  $boss){
            $bosses[$boss->getFullName()] = $boss->id;
            $bosses->forget($key);
        }
        return $bosses->toArray() ? $bosses : ['N/A' => 0];
    }

}