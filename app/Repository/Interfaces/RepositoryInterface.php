<?php
namespace App\Repository\Interfaces;

use Illuminate\Http\Request;

interface RepositoryInterface
{
    function all($columns = ['*']);

    function paginate($quantity);

    function store(Request $request);

    function update(Request $request , $id);

    function delete($id);

    function find($id , $columns = ['*']);

    function findBy($field , $value , $columns = ['*']);
}