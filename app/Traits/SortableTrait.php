<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait SortableTrait
{
    function sortCollection(Collection $collection, array $options)
    {
        $comparer = $this->makeComparer($options);
        return $collection->sort($comparer);
    }

    function sortQuery(array $options , $query = null)
    {
        if(!$query){ //prepare query if it wasnt prepared already
            $query = DB::table($this->getTable());
            $query = $this->prepareQuery($query , $options);
        }

        foreach ($options as $column => $order) { //sort query
            $query->orderBy($column , $order);
        }
        return $query;
    }

    function prepareQuery($query , &$options = null){
        $selectStatement = []; //this is select statement that we will be building
        foreach ((array)$query->columns as $statement){ //get already added columns if there are any and add them to statement
            $selectStatement[] = $statement === '*' ? $this->getTable() . '.*' : $statement;
        }
        $selectStatement ?  : $selectStatement[] = $this->getTable() . '.*'; //if statement is empty add all columns from table else skip
        foreach ($this->joinColumns as $key => $rules){
            $query->join(
                $rules['table'] . ' as ' . $rules['alias'],     // join table name as alias
                $rules['alias'] . '.' . $rules['primary_key'], // on alias.primary_key
                $rules['operator'],                           // operator_type e.g. = , < , >, etc.
                $this->getTable() . '.' . $key               // table_name.foreign_key
            );
            foreach ($rules['select'] as $columnName => $columnAlias){ //add select statements for joined columns
                $selectStatement[] = $rules['alias'] . '.' . $columnName . ' as ' . $columnAlias;
            }
            $options = $this->replaceKey($options , $key , array_values($rules['select'])); //edit options for newly joined columns
        }
        return $query->select($selectStatement);
    }

    private function replaceKey($arr , $key , $newKeys){
        $newArray = [];
        foreach ((array)$arr as $oldKey => $value){
            if($key === $oldKey){
                foreach ($newKeys as $newKey){
                    $newArray[$newKey] = $value;
                }
            }
            else{
                $newArray[$oldKey] = $value;
            }
        }
        return $newArray;
    }

    private function makeComparer($options){
        $comparer = function ($first, $second) use ($options) {
            foreach ($options as $column => $order) {
                $order = strtolower($order);
                if ($first[$column] < $second[$column]) {
                    return $order === "asc" ? -1 : 1;
                } else if ($first[$column] > $second[$column]) {
                    return $order === "asc" ? 1 : -1;
                }
            }
            return 0;
        };
        return $comparer;
    }
}