<?php
namespace App\Traits;

use TomLingham\Searchy\Facades\Searchy;

trait SearchableTrait
{
    function search(String $query)
    {
        return Searchy::driver('simple')->search($this->getTable())->fields($this->searchableColumns)->query($query)->getQuery();
    }
}
