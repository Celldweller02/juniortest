<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAuthenticated
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return $next($request);
        }
        elseif ($request->ajax()){
            return response()->json(['danger' => 'You must sign in']);
        }

        return redirect()->route('login.get');
    }
}