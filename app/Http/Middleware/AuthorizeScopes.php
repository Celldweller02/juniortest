<?php

namespace App\Http\Middleware;

use Closure;
use \Response;

class AuthorizeScopes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $scopes = $request->get('scope');
        $parsedScope = explode(' ' , $scopes);
        $userRole = request()->user()->getRole();
        $scopesByRole = config('scopes.' . $userRole);
        $declinedScopes = null;
        foreach ($parsedScope as $scope){
            if($scope === '*'){
                $request->merge([
                    'scope' => implode(' ' , $scopesByRole),
                ]);
                $declinedScopes = null;
                break;
            }
            if(!in_array($scope , (array)$scopesByRole)){
                $declinedScopes = $declinedScopes ? ' ' . $scope : $scope;
            }
        }
        if($declinedScopes){
            return Response::json([
                'error' => [
                    'message' => "You are not authorized to use these scopes: " . $declinedScopes
                ],
            ] , 403);
        }

        return $next($request);
    }
}
