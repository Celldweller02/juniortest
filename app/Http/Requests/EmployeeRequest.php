<?php
namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest as Request;

class EmployeeRequest extends Request
{
    public function rules(){
        return [
            'last_name' => 'required|string|min:1|max:99',
            'first_name' => 'required|string|min:1|max:99',
            'middle_name' => 'required|string|min:1|max:99',
            'hierarchy_level' => 'required|integer|min:0|max:6',
            'hired_at' => 'required|date',
            'salary' => 'required|integer|min:1',
            'parent_id' => 'required|integer|min:0',
            'avatar' => 'nullable|image|max:2000',
        ];
    }

    public function authorize(){
        return true;
    }
}