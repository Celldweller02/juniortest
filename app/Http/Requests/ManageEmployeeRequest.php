<?php
namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest as Request;

class ManageEmployeeRequest extends Request
{
    public function rules(){
        return [
            'search' => 'nullable|string|min:1|max:99',
            'sort' => 'nullable|array',
            'page' => 'nullable|integer|min:1',
        ];
    }

    public function authorize()
    {
        return true;
    }
}