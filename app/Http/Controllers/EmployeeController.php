<?php
namespace App\Http\Controllers;

use App\Exceptions\EmployeeRepositoryException;
use App\Http\Requests\EmployeeFormRequest;
use App\Http\Requests\EmployeeRequest;
use App\Repository\EmployeeRepository as EmployeeManager;
use App\Http\Requests\ManageEmployeeRequest;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    protected $employeeManager;

    function __construct(EmployeeManager $employeeManager)
    {
        $this->employeeManager = $employeeManager;
        $this->middleware('authenticated' , ['except' => 'displaySubordinates']);
    }

    public function displaySubordinates($id){
        $subordinates = $this->employeeManager->getSubordinates($id);
        return view('sub-views.display-subordinates')->with('subordinates' , $subordinates);
    }

    public function manageEmployees(ManageEmployeeRequest $request){
        $employees = $this->employeeManager->manageEmployees($request);
        if($request->ajax() && strtok(url()->current() , '?') == strtok(url()->previous() , '?')){
            $view = view('sub-views.display-sorted-employees')->with('employees' , $employees)->render();
            return response()->json($view);
        }
        return view('employees-manage')->with('employees' , $employees);
    }

    public function update($id , EmployeeRequest $request){
        try{
            $employee = $this->employeeManager->find($id);
        } catch (\Exception $e){
            throw new EmployeeRepositoryException('Employee with id: ' .$id . ' can not be found.' , 404);
        }
        $employee = $this->employeeManager->storeEmployee($request , $employee);
        return response()->json(['success' => 'Success'] , 200);
    }

    public function store(EmployeeRequest $request){
        $employee = $this->employeeManager->storeEmployee($request);
        return response()->json(['success' => 'Success'] , 200);
    }

    public function delete($id){
        try{
            $employee = $this->employeeManager->find($id);
        } catch (\Exception $e){
            throw new EmployeeRepositoryException('Employee with id: ' .$id . ' can not be found.' , 404);
        }
        $this->employeeManager->deleteEmployee($employee);
        return response()->json(['success' => 'Employee was deleted'] , 200);
    }

    public function getEmployeeForm(EmployeeFormRequest $request){
        return $this->employeeManager->prepareEmployeeForm($request);
    }

    public function changeBoss(Request $request){
        $employeeId = $request->get('employeeId');
        $bossId = $request->get('bossId');
        return $this->employeeManager->changeBoss($employeeId , $bossId);
    }
}