<?php

namespace App\Http\Controllers;

use App\Exceptions\EmployeeRepositoryException;
use App\Repository\EmployeeRepository as EmployeeManager;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{
    protected $employeeManager;

    function __construct(EmployeeManager $employeeManager)
    {
        $this->employeeManager = $employeeManager;
    }

    function showAvatar($id , Request $request){
        $width = (int)$request->get('resize-x');
        $height = (int)$request->get('resize-y');
        $image = null;
        try{
            $employee = $this->employeeManager->find($id);
        } catch (\Exception $exception){
            throw new EmployeeRepositoryException('Employee with id: ' . $id . ' can not be found');
        }
        if($employeeAvatar = $employee->avatar){
            $image = Image::make($employeeAvatar);
        }
        else{
            $image = Image::make(public_path('img/default_user.jpg'));
        }
        if($width || $height){
            $image->resize($width , $height);
        }
        return $image->response();
    }
}