<?php

namespace App\Http\Controllers\API;


use App\Employee;
use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\ManageEmployeeRequest;
use App\Http\Resources\EmployeeCollection;
use App\Http\Resources\EmployeeResource;
use App\Repository\EmployeeRepository;
use Mockery\Exception;

class EmployeeAPIController extends APIController
{
    protected $employeeManager;

    public function __construct(EmployeeRepository $employeeManager)
    {
        $this->employeeManager = $employeeManager;
    }

    public function show($id){
        try{
            $employee = new EmployeeResource($this->employeeManager->find($id));
        } catch (Exception $e){
            return $this->respondNotFound('Employee with id: ' . $id . ' was not found.');
        }
        return $this->respond($employee);
    }

    public function list(ManageEmployeeRequest $request){
        return new EmployeeCollection($this->employeeManager->manageEmployees($request));
    }

    public function store(EmployeeRequest $request){
        try{
            $employee = $this->employeeManager->storeEmployee($request);
        } catch (Exception $e){
            return $this->setStatusCode($e->getCode())->respondWithError($e->getMessage());
        }
        return $this->respond(new EmployeeResource($employee));
    }

    public function update($id , EmployeeRequest $request){
        try{
            $employee = $this->employeeManager->find($id);
        } catch(Exception $e){
            return $this->respondNotFound('Employee with id: ' . $id . ' can not be found');
        }

        try{
            $employee = $this->employeeManager->storeEmployee($request , $employee);
        } catch (Exception $e){
            return $this->setStatusCode($e->getCode())->respondWithError($e->getMessage());
        }
        return $this->respond(new EmployeeResource($employee));
    }

    public function delete($id){
        try{
            $employee = $this->employeeManager->find($id);
        } catch(Exception $e){
            return $this->respondNotFound('Employee with id: ' . $id . ' can not be found');
        }

        $success = $this->employeeManager->deleteEmployee($employee);

        if($success){
            return $this->respondWithMessage("Employee with id: " . $id . " was deleted");
        }
        return $this->respondInternalServerError("Employee with id: " . $id . " was not deleted");
    }
}