<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use \Response;
use Illuminate\Http\Response as IlluminateResponse;

class APIController extends Controller
{
    /**
     * @var int
     */
    private $statusCode = 200;

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     * @return $this
     */
    public function setStatusCode(int $statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param $data
     * @param array $headers
     * @return mixed
     */
    public function respond($data , $headers = []){
        return Response::json($data , $this->getStatusCode() , $headers);
    }

    /**
     * @param $message
     * @return mixed
     */
    public function respondWithMessage($message){
        return $this->respond([
            'message' => $message
        ]);
    }

    /**
     * @param $message
     * @return mixed
     */
    protected function respondWithError($message){
        return $this->respond([
            'error' => [
                'message' => $message,
            ],
        ]);
    }

    /**
     * @param $message
     * @return mixed
     */
    public function respondNotFound($message){
        return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)
            ->respondWithError($message);
    }

    /**
     * @param $message
     * @return mixed
     */
    public function respondInternalServerError($message){
        return $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)
            ->respondWithError($message);
    }
}