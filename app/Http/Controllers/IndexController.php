<?php

namespace App\Http\Controllers;

use App\Repository\EmployeeRepository as EmployeeManager;

class IndexController extends Controller
{
    protected $employeeManager;

    function __construct(EmployeeManager $employeeManager)
    {
        $this->employeeManager = $employeeManager;
        $this->middleware('authenticated' , ['only' => 'manageEmployees']);
    }

    public function index(){
        $roots = $this->employeeManager->getRoots();
        return view('index')->with('roots' , $roots);
    }

}