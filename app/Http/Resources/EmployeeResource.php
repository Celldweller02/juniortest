<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'last_name' => $this->last_name,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'position' => $this->position,
            'hired_at' => $this->hired_at,
            'salary' => $this->salary,
            'boss' => $this->isRoot() ? "N/A" : $this->parent->getFullName(),
            'avatar' => $this->avatar ? "http://junior.test/avatar/" . $this->id : "",
        ];
    }
}
