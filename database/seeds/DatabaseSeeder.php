<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seedingData = config('seeding.hierarchy');

        $start = microtime(true);

        $parent_keys = collect([0]);

        foreach ($seedingData as $hierarchy => $value){
            if($hierarchy > 0){
                $parent_hierarchy = $hierarchy - 1;
                $parent_keys = DB::table('employees')->where('hierarchy_level' , $parent_hierarchy)->pluck('id');
            }
            while ($parent_keys->count() > 0){
                $parent_key = $parent_keys->shift();
                factory(App\Employee::class , $value['quantity'])->create([
                    'position' => $value['position'],
                    'hierarchy_level' => $hierarchy,
                    'salary' => $value['salary'],
                    'parent_id' => $parent_key,
                ]);
            }
        }

        $end = microtime(true);

        print_r($end - $start);

    }
}
