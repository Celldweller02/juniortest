<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('employees')->middleware('auth:api')->group(function (){

    Route::get('/{id}' , 'EmployeeAPIController@show')->middleware('scope:get-employee');

    Route::get('/' , 'EmployeeAPIController@list')->middleware('scope:get-employee');

    Route::post('/' , 'EmployeeAPIController@store')->middleware('scope:store-employee');

    Route::put('/{id}' , 'EmployeeAPIController@update')->middleware('scope:update-employee');

    Route::delete('/{id}' , 'EmployeeAPIController@delete')->middleware('scope:delete-employee');

});
