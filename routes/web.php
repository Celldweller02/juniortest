<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , 'IndexController@index')->name('home');
Route::get('subordinates/{id}' , 'EmployeeController@displaySubordinates')->name('subordinates.get')->where('id' , '[0-9]+');
Route::get('manage/employees' , 'EmployeeController@manageEmployees')->name('employees.manage');
Route::get('login' , function () {
    return view('login');
})->name('login.get')->middleware('guest');
Route::post('login' , 'Auth\LoginController@loginUser')->name('login.post');
Route::get('logout' , 'Auth\LoginController@logoutUser')->name('logout');
Route::get('register' , function (){
    return view('register');
})->name('register.get');
Route::post('register' , 'Auth\RegisterController@registerUser')->name('register.post');
Route::post('employees/update/{id}' , 'EmployeeController@update')->name('employee.update')->where('id' , '[0-9]+');
Route::post('employees/add' , 'EmployeeController@store')->name('employee.add');
Route::get('employees/delete/{id}' , 'EmployeeController@delete')->name('employee.delete')->where('id' , '[0-9]+');
Route::get('get-employee-form/{id?}' , 'EmployeeController@getEmployeeForm')->name('employee.form')->where('id' , '[0-9]+');
Route::get('avatar/{id}' , 'ImageController@showAvatar')->name('avatar.show');
Route::post('employees/change-boss' , 'EmployeeController@changeBoss')->name('employee.change-boss');
Route::get('auth/callback' , 'TestController@index');

