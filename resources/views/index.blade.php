@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row" id="tree">
            <h1>Our Team</h1>
            <ul>
               @foreach($roots as $root)
                    <li class="droppable" data-id="{{$root->id}}" data-position="{{ $root->hierarchy_level }}" data-fetched="1">
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object" src="{{ route('avatar.show', ['id' => $root->id]) . '?resize-x=32&resize-y=32' }}" style="width: 32px;height: 32px" alt="...">
                            </div>
                            <div class="media-body media-middle">
                                {{ $root->getFullName() }}
                                <strong>{{ $root->position }}</strong>
                                <a class="subordinates" href="#" data-display='1' style="margin-left: 10px">Hide subordinates</a>
                            </div>
                        </div>
                        <ul>@include('sub-views.display-subordinates' , ['subordinates' => $root->children])</ul>
                    </li>
               @endforeach
            </ul>
        </div>
    </div>
    <script>
        $('#tree').on('click' , '.subordinates' , function () { //fix adding by drag-n-drop without clicking show button first
            var parent = $(this).closest('li');
            var list = parent.find('ul:first');
            if (+parent.attr('data-fetched')){
                list.slideToggle();
                changeDisplay($(this));
            }
            else {
                $.ajax({
                    type: 'GET',
                    url : $(this).data('url'),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success : function (data) {
                        list.append(data);
                    }
                });
                parent.attr('data-fetched' , '1');
                changeDisplay($(this));
            }
            return false;
        })

        function changeDisplay(element) {
            element.data('display')
                ? element.html('Show subordinates').data('display' , 0)
                : element.html('Hide subordinates').data('display' , 1);
        }

        DragManager.onDragCancel = function(dragObject) {
            dragObject.avatar.rollback();
        };

        DragManager.onDragEnd = function(dragObject, dropElem) {
            dragObject.avatar.rollback();
            if(!dropElem.querySelector('ul')){
                dropElem.appendChild(document.createElement('ul'));
            }
            dropZone = dropElem.querySelector('ul')
            dropZone.insertBefore(dragObject.elem , dropZone.firstChild);
            $.ajax({
                type: 'POST',
                url : '/employees/change-boss',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    employeeId : dragObject.elem.dataset.id,
                    bossId : dropElem.dataset.id
                },
                success : function (data) {
                    if(data.success){
                        if(!+dropElem.dataset.fetched){
                            dragObject.avatar.rollback();
                            dragObject.elem.remove();
                        }
                        alert(data.success);
                    }
                    else{
                        dragObject.avatar.rollback();
                        alert(data.danger);
                    }
                },
                error: function (jXHR , status , error) {
                    dragObject.avatar.rollback();
                    alert(error);
                }
            });
        };

        DragManager.onDroppableEnter = function(dropElem , allowed){
            if(allowed){
                $(dropElem).find(".media-middle:first").removeClass('color-gray').addClass('color-green');
            }
            else{
                $(dropElem).find(".media-middle:first").removeClass('color-gray').addClass('color-red');
            }
        };

        DragManager.onDroppableLeave = function(dropElem){
            $(dropElem).find(".media-middle:first").removeClass('color-green').removeClass('color-red').addClass('color-gray');
        };

        DragManager.constraints.push(new Function('dragElem , dropElem' , 'return +dragElem.dataset.position - +dropElem.dataset.position === 1;'));
    </script>
@endsection