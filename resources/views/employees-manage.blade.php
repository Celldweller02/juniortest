@extends('layouts.main')

@section('content')
    <script>
        $(document).on('click' , 'th' , function () {
            if(!$(this).data('order')){
                $(this).data('order' , 'asc').find('i').removeClass('fa-sort').addClass('fa-sort-up');
            }
            else {
                $(this).data('order') == 'asc'
                    ? $(this).data('order' , 'desc').find('i').removeClass('fa-sort-up').addClass('fa-sort-down')
                    : $(this).data('order' , 'asc').find('i').removeClass('fa-sort-down').addClass('fa-sort-up');
            }
            setSortForm($(this).data('name') , $(this).data('order'));
        });

        $(document).on('click' , '.pagination a' , function (e) {
            e.preventDefault();
            var form = $('#sort-search-form');
            var pageInput = form.find('input[name="page"]');
            var pageNumber = $(this).html();
            if(rel = $(this).attr('rel')){
                pageNumber = rel === 'prev'
                    ? parseInt($('.pagination .active span').html()) -1
                    : parseInt($('.pagination .active span').html()) +1;
            }
            if(pageInput.length){
              pageInput.attr('value' , pageNumber);
            }
            else{
                form.append('<input type="text" name="page" style="display: none" value="' + pageNumber + '">')
            }
            form.submit();
        })

        $(document).on('submit' , '#sort-search-form' , function (e) {
            e.preventDefault();
            var btn = $('button[type="submit"]');
            btn.html("<img class='loader'>");
            $.ajax(
                {
                    type: 'GET',
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    success: function (data) {
                        btn.html("Search/Sort");
                        var table = $('table');
                        table.find('tbody').remove();
                        $('.pagination').remove();
                        table.remove();
                        var pagination = data.substr(data.indexOf('<ul'));
                        $('#employee-table').append(pagination);
                        table.append(data);
                        $('#employee-table').append(table);
                        $('#employee-table').append(pagination);
                    }
                }
            )
        });

        $(document).on('submit' , '#employee-form' , function (e) {
            e.preventDefault();
            var button = $(this).find('button[type="submit"]').html("<img class='loader'>");
            var form = $(this);
            form.find('.help-block').each(function () {
                $(this).remove();
            });
            form.find('.has-error').each(function () {
                $(this).removeClass('has-error');
            });
            var formData = new FormData(this);
            $.ajax(
                {
                    type: 'POST',
                    url: $(this).attr('action'),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Accept' : 'application/json; charset=utf-8'
                    },
                    data: formData,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        button.html("Submit");
                        $('#employee-modal').modal('hide');
                        $('#sort-search-form').submit();
                        printAlert('success' , data.success);
                    },
                    error: function (jXHR , status , error) {
                        button.html("Submit");
                        var defaultAlert = true;
                        var errors = jXHR.responseJSON;
                        $.each(errors , function (key , value) {
                            var element = form.find('input[name="' + key + '"]');
                            if (element.length > 0){
                                defaultAlert = false;
                                element.parent().addClass('has-error');
                                element.after('<span class="help-block"><strong>' + value + '</strong></span>')
                            }
                        });
                        if(errors.error){
                            printAlert('danger' , errors.error);
                        }
                        else if (defaultAlert){
                            printAlert('danger' , error);
                        }
                    }
                }
            )
        });

        function printAlert(type , msg) {
            var alert = $('<div class="alert alert-dismissable"></div>').addClass('alert-' + type).addClass('z-alert')
                .html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + msg);
            $('body').append(alert);
        }

        $(document).on({
            mouseenter : function () {
                $(this).append('<td><button class="edit" type="button" class="btn btn-default">Edit</button></td>');
                $(this).append('<td><button class="delete" type="button" class="btn btn-default">Delete</button></td>');
                $(this).css('background-color' , '#ffffba');
            } ,
            mouseleave : function () {
                $(this).find('td:last-child').remove();
                $(this).find('td:last-child').remove();
                $(this).css('background-color' , 'inherit');
            }
        } , 'tbody tr');

        $(document).on('click' , '.edit , .add-employee' , function () {
            var id = $(this).parents('tr').find('td:first-child').html();
            $.ajax({
                type: 'GET',
                url: '/get-employee-form',
                cache: false,
                data: {
                    id : id
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('body').append(data);
                    $('#employee-modal').modal();
                    $('.date').datepicker({format:'yyyy-mm-dd'});
                }
            });
        });

        $(document).on('click' , '.delete' , function () {
            if(!confirm('Are you sure about this?')){
                return false;
            }
            var id = $(this).parents('tr').find('td:first-child').html();
            $.ajax({
                type: 'GET',
                url: '/employees/delete/' + id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Accept' : 'application/json; charset=utf-8'
                },
                success: function (data) {
                    $('#sort-search-form').submit();
                    printAlert('success' , data.success);
                },
                error: function (jXHR , status , error) {
                    var jsonError = jXHR.responseJSON.error;
                    if(jsonError){
                        printAlert('danger' , jsonError);
                    }
                    else {
                        printAlert('danger' , error);
                    }
                }
            });
        });

        $(document).on('change' , 'select[name="hierarchy_level"]' , function () {
            var hierarchy_level = $(this).find('option:selected').attr('value');
            $.ajax({
                type: 'GET',
                url: '/get-employee-form',
                data: {
                    hierarchy_level : hierarchy_level
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    var bossesSelect = $('select[name="parent_id"]');
                    bossesSelect.empty();
                    Object.keys(data).forEach(function (key) {
                        bossesSelect.append('<option value="'+ data[key] +'">'+ key +'</option>');
                    });
                }
            });
        });

        $(document).on('hidden.bs.modal' , '.modal' , function () {
            $(this).remove();
        });

        function setSortForm(name , order) {
           var form = $('#sort-search-form');
           form.find("input[name='sort[" + name + "]']").remove();
           form.append("<input type='text' name='sort[" + name + "]' value='" + order + "' style='display: none'>");
        }
    </script>

    <div class="container">
        <div class="row">
            <button type="button" class="add-employee  btn btn-default">Add employee</button>
            <div id="employee-table" class="row">
                <table class="table">
                    <div class="col-xs-6 col-md-4 pull-right" style="margin: 22px 0px">
                        <form id="sort-search-form" action="{{ route('employees.manage') }}" method="get">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search...">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default">Search/Sort</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <thead>
                    <tr>
                        <th data-order="false" data-name="id">ID <i class="fas fa-sort"></i></th>
                        <th data-order="false" data-name="last_name">Last Name <i class="fas fa-sort"></i></th>
                        <th data-order="false" data-name="first_name">First Name <i class="fas fa-sort"></i></th>
                        <th data-order="false" data-name="middle_name">Middle Name <i class="fas fa-sort"></i></th>
                        <th data-order="false" data-name="position">Position <i class="fas fa-sort"></i></th>
                        <th data-order="false" data-name="hired_at">Hired At <i class="fas fa-sort"></i></th>
                        <th data-order="false" data-name="salary">Salary <i class="fas fa-sort"></i></th>
                        <th data-order="false" data-name="parent_id">Boss <i class="fas fa-sort"></i></th>
                    </tr>
                    </thead>
                    @include('sub-views.display-sorted-employees' , ['employees' => $employees])
                </table>
                {{ $employees->links() }}
            </div>
        </div>
    </div>
@endsection