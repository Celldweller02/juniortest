
@foreach($subordinates as $subordinate)
    <li class="draggable droppable" data-id="{{$subordinate->id}}" data-position="{{ $subordinate->hierarchy_level }}" data-fetched="0">
        <div class="media">
            <div class="media-left">
                <img class="media-object" src="{{ route('avatar.show', ['id' => $subordinate->id]) . '?resize-x=32&resize-y=32' }}" style="width: 32px;height: 32px" alt="...">
            </div>
            <div class="media-body media-middle">
                {{ $subordinate->getFullName() }}
                <strong>{{ $subordinate->position }}</strong>
                <a class="subordinates" href="#" data-display="0" data-url="{{ route('subordinates.get' , ['id' => $subordinate->id] ) }}" style="margin-left: 10px">Show subordinates</a>
            </div>
        </div>
        <ul></ul>
    </li>
@endforeach