<tbody>
@foreach($employees as $employee)
    <tr>
        <td>{{ $employee->id }}</td>
        <td>{{ $employee->last_name }}</td>
        <td>{{ $employee->first_name }}</td>
        <td>{{ $employee->middle_name }}</td>
        <td>{{ $employee->position }}</td>
        <td>{{ $employee->hired_at }}</td>
        <td>{{ $employee->salary }}</td>
        @if($employee->parent)
            <td>{{ $employee->parent->getFullName() }}</td>
        @else
            <td>N/A</td>
        @endif
    </tr>
@endforeach
</tbody>
{{ $employees->links() }}