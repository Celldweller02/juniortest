<div id="employee-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ isset($employee) ? 'Edit employee' : 'Add employee' }}</h4>
            </div>
            <div class="modal-body">
                <form id="employee-form" action="{{ isset($employee) ? route('employee.update' , ['id' => $employee->id]) : route('employee.add') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <fieldset>
                        <div class="media">
                            <div class="media-middle" style="display: inline-block">
                                <img class="media-object" style="width: 200px;height: 200px;" src="{{ isset($employee) ? route('avatar.show', ['id' => $employee->id]) . '?no-cache=' . time() : '/public/img/default_user.jpg' }}">
                            </div>
                            <div class="media-bottom" style="display:inline-block;">
                                <input type="file" name="avatar" id="avatar" style="visibility: hidden">
                                <label class="btn btn-default" for="avatar">Select avatar</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{ isset($employee) ? $employee->last_name : old('last_name')}}">
                        </div>
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{ isset($employee) ? $employee->first_name : old('first_name') }}">
                        </div>
                        <div class="form-group">
                            <label>Middle Name</label>
                            <input type="text" class="form-control" name="middle_name" placeholder="Middle Name" value="{{ isset($employee) ? $employee->middle_name : old('middle_name') }}">
                        </div>
                        <div class="form-group">
                            <label>Position</label>
                            <select class="form-control" name="hierarchy_level">
                                @foreach($positions as $key => $value)
                                    <option value="{{ $value }}" {{ isset($employee) ? ($employee->hierarchy_level === $value ? 'selected' : '') : '' }} >{{ $key }}</option>
                                @endforeach
                            </select>
                        </div>
                        <label>Hired At</label>
                        <div class="input-group date">
                            <input class="form-control" type="text" name="hired_at" value="{{ isset($employee) ? $employee->hired_at : old('hired_at') }}" placeholder="Date: YYYY-MM-DD" aria-describedby="input-addon">
                            <span class="input-group-addon" id="input-addon"><i class="fas fa-calendar-alt"></i></span>
                        </div>
                        <div class="form-group">
                            <label>Salary</label><br>
                            <input type="text" name="salary" value="{{ isset($employee) ? $employee->salary : old('salary') }}">
                        </div>
                        <div class="form-group">
                            <label>Boss</label>
                            <select class="form-control" name="parent_id">
                                @foreach($bosses as $key => $value)
                                    <option value="{{ $value }}" {{ isset($employee) ? ($employee->parent_id === $value ? 'selected' : '') : '' }} >{{ $key }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default" role="button">Submit</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>