<?php
return [
    'user' => [
        'get-employee',
    ],

    'admin' => [
        'get-employee',
        'store-employee',
        'update-employee',
        'delete-employee',
    ],
];