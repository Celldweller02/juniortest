<?php

return [
    'hierarchy' => [
        0 => [
            'position' => 'CEO',
            'quantity' => 1,
            'salary' => 45000,
        ],
        1 => [
            'position' => 'General Manager',
            'quantity' => 5,
            'salary' => 30000,
        ],
        2 => [
            'position' => 'Regional Manager',
            'quantity' => 12,
            'salary' => 20000,
        ],
        3 => [
            'position' => 'Supervisor',
            'quantity' => 5,
            'salary' => 12000,
        ],
        4 => [
            'position' => 'Project Manager',
            'quantity' => 5,
            'salary' => 10000,
        ],
        5 => [
            'position' => 'Team Lead',
            'quantity' => 5,
            'salary' => 8000,
        ],
        6 => [
            'position' => 'Employee',
            'quantity' => 6,
            'salary' => 2000,
        ],
    ],
];